/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IOperacion;
import Interface.IOperacion2;

/**
 * Representa una matriz de enteros creada aleatoriamente
 * @author madarme
 */
public class Matriz_Aleatorio implements IOperacion, IOperacion2{
    
    private int matriz[][];

    /**
     * Constructor vacío de la clase Matriz_Aleatorio
     */
    public Matriz_Aleatorio() {
    }
    
    /**
     * Crea una matriz de filas x columnas
     * @param filas un entero que representa la cantidad de filas
     * @param columnas  un entero que representa la cantidad de columnas
     */
    public Matriz_Aleatorio(int filas, int columnas) throws Exception {
        
        if(filas<=0 || columnas<=0)
            throw new Exception("Valores no permitidos para crear una matriz");
        
        this.matriz=new int[filas][columnas];
    }
    
    
    /**
     *  Método llena la matriz de elementos aleatorios a partir de ini hasta fin
     * @param ini un entero que representa el límite inicial
     * @param fin un entero que representa el límite final
     */
    public void crearElementos_Aleatorios(int ini, int fin) throws Exception
    {
    if(ini>=fin)
            throw new Exception("No se pueden crear elementos aleatorios");
    
    for(int i=0;i<this.matriz.length;i++)
    {
        for(int j=0;j<this.matriz[i].length;j++)
        {
            //Pueden usar la clase Random() (0-1)--> 5*0,5 =2,5 --> 3
        this.matriz[i][j]=(int) Math.floor(Math.random()*(ini-fin+1)+fin);
        }
    }
        
        
    }

    public int[][] getMatriz() {
        return matriz;
    }

    public void setMatriz(int[][] matriz) {
        this.matriz = matriz;
    }

    @Override
    public String toString() {
        if(this.matriz==null)
            return "No hay elementos";
       String msg="" ;
        for(int vectorCol[]:this.matriz)
        {
            for(int dato:vectorCol)
                msg+=dato+"\t";
         msg+="\n";   
                
        }
     return msg;   
    }

    @Override
    public int getTotal() {
        
    if(this.matriz==null)    
        return 0;
    int total=0;
    for(int vectorCol[]:this.matriz)
        {
            for(int dato:vectorCol)
                total+=dato;
         
                
        }
    return total;
    }

    @Override
    public void interCambio() {
        for (int[] fila : matriz) {
            intercambiarFilas(fila);
        }
    }
      private void swap(int vector[],int i, int j){
        int ant = vector[j];       
        vector[j]=vector[i];
        vector[i]=ant;
    }

    
    private void intercambiarFilas(int vector[]) {
        int j=vector.length-1;
        for(int i=0; i<vector.length/2 ;i++){
           
           swap(vector,i, j);
           j--;

        }
    }
    
    
    
    
}
