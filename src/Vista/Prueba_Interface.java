/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Interface.IOperacion;
import Interface.IOperacion2;
import Negocio.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class Prueba_Interface {
    
    public static void main(String[] args) {
        
          try {
            System.out.println("Por favor digite datos de la matriz:");
            Scanner lector = new Scanner(System.in);
            System.out.println("Por favor digite cantidad de filas:");
            int filas=lector.nextInt();
            System.out.println("Por favor digite cantidad de columnas:");
            int cols=lector.nextInt();
            System.out.println("Por favor digite limiInicial de elementos aleatorios:");
            int ini=lector.nextInt();
            System.out.println("Por favor digite limifinal de elementos aleatorios:");
            int fin=lector.nextInt();
                      
            // :)
            Matriz_Aleatorio myMatriz=new Matriz_Aleatorio(filas, cols);
            myMatriz.crearElementos_Aleatorios(ini, fin);          
           IOperacion interface1=myMatriz; //casting
           System.out.println("Total matriz:"+interface1.getTotal());
          
           System.out.println("Matriz generada: ");  
                System.out.println(myMatriz.toString());
               
                System.out.println("Forma 1: (objetos de la clase e invocando sus métodos) ");                       
                myMatriz.interCambio();
                System.out.println(myMatriz.toString());
                
                System.out.println("FORMA 2: (POLIMORFISMO)");
                IOperacion2 interface2=myMatriz; //casting
                interface2.interCambio();
                System.out.println("Forma 2:"+"\n"+interface2.toString());
                
                System.out.println("FORMA 3: (CASTING DE LA INTERFACE)");
                ((IOperacion2)myMatriz).interCambio();
                System.out.println("Forma 3:"+"\n"+((IOperacion2)myMatriz).toString());
          
          System.out.println("**************************");      
          //Con el objeto tripleta:
          Tripleta tripleta=new Tripleta(3,6,9);
          interface1=tripleta;
          System.out.println("Total tripleta:"+interface1.getTotal());
          
          System.out.println("Tripleta generada: ");  
                System.out.println(tripleta.toString());
                
                System.out.println("");
                System.out.println("Forma 1: (objetos de la clase e invocando sus métodos) ");                            
                tripleta.interCambio();
                System.out.println(tripleta.toString());
                
                
                System.out.println("");
                System.out.println("FORMA 2: (POLIMORFISMO)");
                interface2=tripleta; //casting
                interface2.interCambio();
                System.out.println("Forma 2:"+"\n"+interface2.toString());
                
                System.out.println("");
                System.out.println("FORMA 3: (CASTING DE LA INTERFACE)");
                ((IOperacion2)tripleta).interCambio();
                System.out.println("Forma 3:"+"\n"+((IOperacion2)tripleta).toString());
                
          System.out.println("**************************");
           //Con el objeto Vector_aleatorio:
          Vector_Aleatorio myVector=new Vector_Aleatorio(10);
          interface1=myVector;
          System.out.println("Total vector: "+interface1.getTotal());
          
          
          System.out.println("Vector generada: ");  
                System.out.println(myVector.toString());
                            
                System.out.println("Forma 1: (objetos de la clase e invocando sus métodos) ");                          
                myVector.interCambio();
                System.out.println(myVector.toString());
                
                System.out.println("FORMA 2: (POLIMORFISMO)");
                interface2=myVector; //casting
                interface2.interCambio();
                System.out.println("Forma 2:"+"\n"+interface2.toString());
                
                System.out.println("FORMA 3: (CASTING DE LA INTERFACE)");
                ((IOperacion2)myVector).interCambio();
                System.out.println("Forma 3:"+"\n"+((IOperacion2)myVector).toString());
                
            
        } catch(java.util.InputMismatchException ex2)
        {
            System.err.println("Error en la entrada de datos enteros:"+ex2.getMessage());
        }
            catch (Exception ex) {
            
             // :(
            System.err.println("Error:"+ex.getMessage());
        }
        
    }
    
}
